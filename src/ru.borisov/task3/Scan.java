package ru.borisov.task3;

import java.util.Scanner;

public class Scan {
    public static void main(String[] args) {
        int a = 0;
        boolean isExit = false;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите число (exit для выхода): ");

        while (scanner.hasNext() && !isExit) {
            if (scanner.hasNextInt()) {
                a = scanner.nextInt();
            } else {
                String tmp = scanner.next();
                if ("exit".equals(tmp)) {
                    isExit = true;
                    System.exit(0);
                } else {
                    System.out.println("Неверный ввод");
                    System.out.println("Введите число (exit для выхода): ");
                }
            }
            if (a < 0 & (a % 2 == 0)){
                System.out.println("Число четное отрицательное");

            } else if (a < 0) {
                System.out.println("Число нечетное отрицательное");

            } else if (a == 0) {
                System.out.println("Ноль");

            } else if (a > 0 & (a % 2 == 0)) {
                System.out.println("Число четное положительное");

            } else if (a > 0) {
                System.out.println("Число нечетное положительное");
            }
        }
    }
}
