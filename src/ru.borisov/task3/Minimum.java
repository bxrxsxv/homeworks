package ru.borisov.task3;

import java.util.Scanner;

public class Minimum {
    public static void main(String[] args) {
        double a = 0;
        double b = 0;
        boolean isExit = false;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите первое число (exit для выхода): ");

        while (scanner.hasNext() && !isExit) {
            if (scanner.hasNextDouble()) {
                a = scanner.nextDouble();
                break;
            } else {
                String tmp = scanner.next();
                if ("exit".equals(tmp)) {
                    isExit = true;
                    System.exit(0);
                } else {
                    System.out.println("Неверный ввод");
                    System.out.println("Введите первое число (exit для выхода): ");
                }
            }
        }

        System.out.println("Введите второе число (exit для выхода): ");

        while (scanner.hasNext() && !isExit) {

            if (scanner.hasNextDouble()) {
                b = scanner.nextDouble();
                break;
            } else {
                String tmp = scanner.next();
                if ("exit".equals(tmp)) {
                    isExit = true;
                    System.exit(0);
                } else {
                    System.out.println("Неверный ввод");
                    System.out.println("Введите второе число (exit для выхода): ");
                }
            }
        }
        if(a < b) {
            System.out.println("Первое число меньше второго");
        } else if(a > b) {
            System.out.println("Второе число меньше первого");
        } else {
            System.out.println("Числа равны");
        }
    }
}
