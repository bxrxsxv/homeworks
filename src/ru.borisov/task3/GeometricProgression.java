package ru.borisov.task3;

import java.util.Scanner;

public class GeometricProgression {
    public static void main(String[] args) {
        double number = 0;
        double last = 0;
        double step = 0;
        double res;
        boolean isExit = false;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите первый член прогрессии а1 (exit для выхода): ");

        while (scanner.hasNext() && !isExit) {
            if (scanner.hasNextDouble()) {
                number = scanner.nextDouble();
                break;
            } else {
                String tmp = scanner.next();
                if ("exit".equals(tmp)) {
                    isExit = true;
                    System.exit(0);
                } else {
                    System.out.println("Неверный ввод");
                    System.out.println("Введите первый член прогрессии а1 (exit для выхода): ");
                }
            }
        }

        System.out.println("Введите знаменатель прогрессии (exit для выхода): ");

        while (scanner.hasNext() && !isExit) {
            if (scanner.hasNextDouble()) {
                step = scanner.nextDouble();
                break;
            } else {
                String tmp = scanner.next();
                if ("exit".equals(tmp)) {
                    isExit = true;
                    System.exit(0);
                } else {
                    System.out.println("Неверный ввод");
                    System.out.println("Введите знаменатель прогрессии (exit для выхода): ");
                }
            }
        }

        System.out.println("Введите номер последнего члена (exit для выхода): ");

        while (scanner.hasNext() && !isExit) {
            if (scanner.hasNextDouble()) {
                last = scanner.nextDouble();
                break;
            } else {
                String tmp = scanner.next();
                if ("exit".equals(tmp)) {
                    isExit = true;
                    System.exit(0);
                } else {
                    System.out.println("Неверный ввод");
                    System.out.println("Введите номер последнего члена (exit для выхода): ");
                }
            }
        }

        res = number * Math.pow(step, (last - 1));

        while (number < res) {
            number = number * step;
            System.out.println(number);
        }
        System.out.println("Последний член аn: " + res);
    }
}
