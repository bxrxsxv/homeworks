package ru.borisov.task11;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ProgramOne {
    public static void main(String[] args) throws IOException {
        Path fileOne = Paths.get("C:/Users/Admin/IdeaProjects/Innopolis/src/ru.borisov/task11/fileOne.txt");
        Path fileTwo = Paths.get("C:/Users/Admin/IdeaProjects/Innopolis/src/ru.borisov/task11/fileTwo.txt");
        if (!Files.exists(fileOne)) {
            Files.createFile(fileOne);
            System.out.println("Создаем файл: fileOne.txt");
        }
        if (Files.exists(fileOne)) {
            Files.copy(fileOne, fileTwo);
            Files.delete(fileOne);
            System.out.println("Копируем файл fileOne.txt в fileTwo.txt и удаляем fileOne.txt");
        }
        if (Files.exists(fileTwo)) {
            Files.move(fileTwo, fileTwo.resolveSibling("newName.txt"));
            System.out.println("Переименовываем файл fileTwo.txt в newName.txt");
        }
    }
}
