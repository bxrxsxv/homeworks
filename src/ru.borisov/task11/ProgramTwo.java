package ru.borisov.task11;

import java.io.File;

public class ProgramTwo {
    private static String counterDir = "-";
    public static void main(String[] args) {
        searchFiles(new File("C:\\Users\\Admin\\IdeaProjects\\Innopolis\\dir"), counterDir);
    }

    private static void searchFiles(File rootFile, String counterDir) {
        if (rootFile.isDirectory()) {
            File[] directoryFiles = rootFile.listFiles();
            if (directoryFiles != null) {
                for (File file : directoryFiles) {
                    if (file.isDirectory()) {
                        System.out.println(counterDir + file.getName());
                        counterDir += "-";
                        searchFiles(file, counterDir);
                    } else {
                        if (file.isFile()) {
                            System.out.println(counterDir + file.getName());
                        }
                    }
                }
            }
        }
    }
}
