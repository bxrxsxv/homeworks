package ru.borisov.task5;

import java.util.Scanner;


public class App {
    private static VendingMachine vendingMachine = new VendingMachine();

    public static void main(String[] args) {
        System.out.println("Выберите напиток из списка: ");
        for(String menu : vendingMachine.menu()) {
            System.out.println(menu);
        }

        Scanner scan = new Scanner(System.in);
        printHelp();
        while (scan.hasNext()) {
            String command = scan.next();
            switch (command) {
                case "add": {
                    int money = scan.nextInt();
                    processAddMoney(money);
                    break;
                }
                case "get": {
                    int key = scan.nextInt();
                    processGetDrink(key);
                    break;
                }
                case "end": {
                    processEnd();
                    return;
                }
                default:
                    System.out.println("Команда не определена");
            }
            scan.nextLine();
        }
    }
    private static void processAddMoney(int money) {
        System.out.println("Текущий баланс: " + vendingMachine.addMoney(money));
    }
    private static void processGetDrink(int number) {
        Drinks drinks = vendingMachine.getDrink(number);
        if(drinks != null) {
            System.out.println("Ммм! " + drinks.getTitle() + "!");
        } else {
            System.out.println("Напиток почему-то не получен...");
        }
    }
    private static void processEnd() {
        System.out.println("Ваша сдача: " + vendingMachine.getChange());
    }

    private static void printHelp() {
        System.out.println("Введите 'add <количество>' для добавления купюр");
        System.out.println("Введите 'get <код напитка>' для получения напитка");
        System.out.println("Введите 'end' для получение сдачи");
    }
}
