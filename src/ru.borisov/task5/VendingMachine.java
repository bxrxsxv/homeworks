package ru.borisov.task5;

public class VendingMachine {
    private int money;

    Drinks[] drinks = new Drinks[]
            {       Drinks.COCACOLA,
                    Drinks.SPRITE,
                    Drinks.FANTA,
                    Drinks.LIPTON,
                    Drinks.NESTEA,
                    Drinks.BONAQUA,
                    Drinks.AQVERY,
                    Drinks.VITTEL};

    public String[] menu() {
        String[] result = new String[drinks.length];
        for (int i = 0; i < drinks.length; i++) {
            result[i] = String.format("%d - %s: %d руб", i, drinks[i].getTitle(), drinks[i].getPrice());
        }
        return result;
    }

    public double addMoney(int money) {
        this.money += money;
        return this.money;
    }

    public Drinks getDrink(int number) {
        if (!isNumberValid(number)) {
            return null;
        }

        Drinks selected = drinks[number];
        if(!isMoneyEnough(selected)) {
            return null;
        }

        Drinks drink = selected;
        money -= drink.getPrice();
        return drink;
    }
    private boolean isMoneyEnough(Drinks selected) {
        return money >= selected.getPrice();
    }

    private boolean isNumberValid (int number) {
        return number >= 0 && number < drinks.length;
    }
    public double getChange() {
        double money = this.money;
        this.money = 0;
        return money;
    }
}

