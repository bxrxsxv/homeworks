package ru.borisov.task5;

import lombok.Getter;

enum Drinks {
    COCACOLA("Coca-cola", 60),
    SPRITE( "Sprite", 60),
    FANTA( "Fanta", 60),
    NESTEA( "Nestea", 55),
    LIPTON( "Lipton", 55),
    BONAQUA( "Bonaqua", 35),
    VITTEL( "Vittel", 50),
    AQVERY( "Aqvery", 50);

    Drinks(String title, int price) {
        this.title = title;
        this.price = price;
    }
    @Getter
    private String title;
    @Getter
    private int price;



}
