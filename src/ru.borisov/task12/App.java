package ru.borisov.task12;

import java.util.ArrayList;
import java.util.List;

import static ru.borisov.task12.Library.*;

public class App {
    public static void main(String[] args) throws Exception{

        Library library = new Library("Library.bin");
        List<Book> books = new ArrayList<>();
        Book book1 = new Book("Игра Престолов", "Джордж Мартин", 1996);
        books.add(book1);
        Book book2 = new Book("Властелин Колец", "Дж. Р. Р. Толкин", 1954);
        books.add(book2);
        Book book3 = new Book("Сага о Ведьмаке", "Анджей Сапковский", 1986);
        books.add(book3);

        library.addBook(books);

        saveInFile(library);

        Library.loadLibrary(String.valueOf(library));

        library.listBooks();
    }
}
