package ru.borisov.task12;

import lombok.Data;

import java.io.Serializable;
@Data
public class Book implements Serializable {
    private String name;
    private String author;
    private int year;

    public Book(String name, String author, int year) {
        this.name = name;
        this.author = author;
        this.year = year;
    }
}
