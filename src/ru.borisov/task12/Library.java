package ru.borisov.task12;

import lombok.Getter;
import lombok.Setter;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Library implements Serializable {
    private static final long serialVersionUID = 123456L;
    @Getter
    @Setter
    private final String libraryFile;
    @Getter
    @Setter
    private List<Book> books = new ArrayList<>();

    public Library(String libraryFile) {
        this.libraryFile = libraryFile;
    }

    public void addBook(List<Book> book) {
        List <Book> newBook = new ArrayList<>();
        newBook.addAll(book);

    }

    public void listBooks() {
        for (Book book : books) {
            System.out.println(book);
        }
    }

    public static void saveInFile(Library library) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(library.getLibraryFile()))) {
                oos.writeObject(library);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Library loadLibrary(String libraryFile) throws IOException, ClassNotFoundException {
        Library result;
        if (!Files.exists(Paths.get(libraryFile))) {
            System.out.println("Файл не найден");
            return new Library(libraryFile);
        }
        try (InputStream is = new FileInputStream(libraryFile);
             BufferedInputStream bis = new BufferedInputStream(is, 100);
             ObjectInputStream ois = new ObjectInputStream(bis)) {
            result = (Library) ois.readObject();
        }
        return result;
    }
}
