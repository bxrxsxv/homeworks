package ru.borisov.task9;

public class Child {
    static Food[] goodFood = {Food.APPLE, Food.CARROT};

    public static void eat(Food food) throws BadEatException {
        try {
            for (Food gf : goodFood) {
                if (gf.equals(food)) {
                    System.out.println();
                    return;
                }
            }
            throw new BadEatException("Фуу каша");
        } finally {
            System.out.println("спасибо, мама");
        }
    }
}
