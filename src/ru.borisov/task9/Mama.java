package ru.borisov.task9;

import java.util.Random;

public class Mama {
    private static Food zavtrak = Food.getFoodRandom();

    public static void kormit() {
        try {
            Child.eat(zavtrak);
        } catch (BadEatException e) {
            System.out.println("В следующий раз приготовлю что нибудь вкусное");
        }
    }

}
