package ru.borisov.task9;

import lombok.Getter;

import java.io.Serializable;

public enum Food {
    CARROT,
    APPLE,
    KASHA;

    public static Food getFoodRandom() {
        Food foodRandom = Food.values()[(int)(Math.random()*Food.values().length)];
        return foodRandom;
    }
}
