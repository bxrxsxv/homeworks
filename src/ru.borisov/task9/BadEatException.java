package ru.borisov.task9;

public class BadEatException extends Exception{
    public BadEatException() {
    }

    public BadEatException(String message) {
        super(message);
    }

    public BadEatException(String message, Throwable cause) {
        super(message, cause);
    }

    public BadEatException(Throwable cause) {
        super(cause);
    }

    public BadEatException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
