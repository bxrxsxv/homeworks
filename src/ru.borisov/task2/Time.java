package ru.borisov.task2;

import java.util.Scanner;

public class Time {
    public static void main(String[] args) {
        double sec = 0;
        double hour;
        boolean isExit = false;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите число секунд: ");

        while (scanner.hasNext() && !isExit) {
            if (scanner.hasNextDouble()) {
                sec = scanner.nextDouble();
                break;
            } else {
                String tmp = scanner.next();
                if ("exit".equals(tmp)) {
                    isExit = true;
                    System.exit(0);
                } else {
                    System.out.println("Неверный ввод");
                    System.out.println("Введите число секунд (exit для выхода): ");
                }
            }
        }
        hour = sec / 3600;

        System.out.println(hour);
    }
}
