package ru.borisov.task2;

import java.util.Scanner;

public class Salary {
    public static void main(String[] args) {
        double sal = 0;
        double after;
        boolean isExit = false;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите зарплату (exit для выхода): ");
        while (scanner.hasNext() && !isExit) {
            if (scanner.hasNextDouble()) {
                sal = scanner.nextDouble();
                break;
            } else {
                String tmp = scanner.next();
                if ("exit".equals(tmp)) {
                    isExit = true;
                    System.exit(0);
                } else {
                    System.out.println("Неверный ввод");
                    System.out.println("Введите зарплату (exit для выхода): ");
                }
            }
        }
        after = 0.87 * sal;

        System.out.println("Зарплата с учетом налога в 13%: " + after);
    }
}
