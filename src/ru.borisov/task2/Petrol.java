package ru.borisov.task2;

import java.util.Scanner;

public class Petrol {
    public static void main(String[] args) {
        double price = 0;
        double liters = 0;
        double cost;
        boolean isExit = false;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите цену за литр (exit для выхода): ");
        while (scanner.hasNext() && !isExit) {
            if (scanner.hasNextDouble()) {
                price = scanner.nextDouble();
                break;
            } else {
                String tmp = scanner.next();
                if ("exit".equals(tmp)) {
                    isExit = true;
                    System.exit(0);
                } else {
                    System.out.println("Неверный ввод");
                    System.out.println("Введите цену за литр (exit для выхода): ");
                }
            }
        }
        System.out.println("Введите количество литров (exit для выхода): ");

        while (scanner.hasNext() && !isExit) {
            if (scanner.hasNextDouble()) {
                liters = scanner.nextDouble();
                break;
            } else {
                String tmp = scanner.next();
                if ("exit".equals(tmp)) {
                    isExit = true;
                    System.exit(0);
                } else {
                    System.out.println("Неверный ввод");
                    System.out.println("Введите количество литров (exit для выхода): ");
                }
            }
        }
        cost = price * liters;

        System.out.println("К оплате: " + cost);
    }
}
