package ru.borisov.task2;

import java.util.Scanner;

public class HotCold {
    public static void main(String[] args) {
        int a = (int) (Math.random() * (100 + 1) + 1);
        System.out.println(a);
        int number = 0;

        boolean isExit = false;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите число (exit для выхода):");

        while (scanner.hasNext() && !isExit) {
            if (scanner.hasNextInt()) {
                number = scanner.nextInt();

            } else {
                String tmp = scanner.next();
                if ("exit".equals(tmp)) {
                    isExit = true;
                    System.exit(0);
                } else {
                    System.out.println("Неверный ввод");
                    System.out.println("Введите число (exit для выхода): ");
                }
            }
            if (0 <= number && number <= 100) {
                int raznica = a - number;
                if (number == a) {
                    System.out.println("Вы угадали число");
                    break;
                } else if (number < a & raznica > 50) {
                    System.out.println("Очень холодно");
                } else if (number < a & raznica > 40) {
                    System.out.println("Холодно");
                } else if (number < a & raznica > 30) {
                    System.out.println("Теплее");
                } else if (number < a & raznica > 20) {
                    System.out.println("Тепло");
                } else if (number < a & raznica > 10) {
                    System.out.println("Почти горячо");
                } else if (number < a & raznica < 10) {
                    System.out.println("Горячо!!!");
                }
                if (0 <= number && number <= 100) {
                    int raznicaMin = number - a;
                    if (number == a) {
                        System.out.println("Вы угадали число");
                        break;
                    } else if (number > a & raznicaMin > 50) {
                        System.out.println("Очень холодно");
                    } else if (number > a & raznicaMin > 40) {
                        System.out.println("Холодно");
                    } else if (number > a & raznicaMin > 30) {
                        System.out.println("Теплее");
                    } else if (number > a & raznicaMin > 20) {
                        System.out.println("Тепло");
                    } else if (number > a & raznicaMin > 10) {
                        System.out.println("Почти горячо");
                    } else if (number > a & raznicaMin < 10) {
                        System.out.println("Горячо!!!");
                    }
                }
            }
            System.out.println("Введите число (exit для выхода):");
        }
    }
}
