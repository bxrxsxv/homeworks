package ru.borisov.task23;

import java.util.ArrayList;
import java.util.List;

public class BasketImpl implements Basket {

    List<Product> list = new ArrayList();

    @Override
    public void addProduct(String product, int quantity) {
        list.add(new Product(product, quantity));
    }

    @Override
    public void removeProduct(String product) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getProductName().equals(product)) {
                list.remove(i);
            }
        }
    }

    @Override
    public void updateProductQuantity(String product, int quantity) {
        list.add(new Product(product, quantity));
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public List<String> getProducts() {
        List<String> stringList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            stringList.add(list.get(i).getProductName());
        }
        return stringList;
    }

    @Override
    public int getProductQuantity(String product) {
        int a = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getProductName().equals(product)) {
                a = list.get(i).getQuantity();
            }
        }
        return a;
    }

    @Override
    public String toString() {
        return "BasketImpl{" +
                "list=" + list +
                '}';
    }
}
