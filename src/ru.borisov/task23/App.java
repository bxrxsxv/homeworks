package ru.borisov.task23;

public class App {
    public static void main(String[] args) {
        BasketImpl basket = new BasketImpl();

        basket.addProduct("Хлеб", 1);
        System.out.println(basket.getProductQuantity("Хлеб"));
        System.out.println(basket.getProducts());
        basket.addProduct("Сахар", 2);
        System.out.println(basket.getProductQuantity("Хлеб"));
        System.out.println(basket.getProductQuantity("Сахар"));
        System.out.println(basket.getProducts());
        basket.addProduct("Сыр", 1);
        System.out.println(basket.getProductQuantity("Хлеб"));
        System.out.println(basket.getProducts());
    }
}
