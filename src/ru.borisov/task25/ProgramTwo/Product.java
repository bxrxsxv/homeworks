package ru.borisov.task25.ProgramTwo;

public class Product {
    String productName;
    int quantity;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Product(String productName, int quantity) {
        this.productName = productName;
        this.quantity = quantity;
    }
}
