package ru.borisov.task25.ProgramTwo;

import java.util.*;

public class BasketImpl implements Basket {

    Map<String, Integer> basket = new HashMap<>();

    @Override
    public void addProduct(String product, int quantity) {
        basket.put(product, quantity);
    }

    @Override
    public void removeProduct(String product) {
        basket.remove(product);
    }

    @Override
    public void updateProductQuantity(String product, int quantity) {
        basket.put(product, quantity);
    }

    @Override
    public void clear() {
        basket.clear();
    }

    @Override
    public List<String> getProducts() {

        return new ArrayList<String>(basket.keySet());
    }

    @Override
    public Integer getProductQuantity(String product) {
        Integer a = 0;
        for (Map.Entry<String, Integer> entry : basket.entrySet()) {
            String key = entry.getKey();
            Integer value = entry.getValue();

            if (key.equals(product)) {
                a = value;
            }
        }
        return a;
    }

    @Override
    public String toString() {
        return "BasketImpl{" +
                "basket=" + basket +
                '}';
    }
}
