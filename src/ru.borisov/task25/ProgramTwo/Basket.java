package ru.borisov.task25.ProgramTwo;

import java.util.List;

public interface  Basket {
    void addProduct(String product, int quantity);

    void removeProduct(String product);

    void updateProductQuantity(String product, int quantity);

    void clear();

    List<String> getProducts();

    Integer getProductQuantity(String product);
}
