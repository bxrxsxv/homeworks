package ru.borisov.task7.Animal;

public class Duck extends Animal implements Run, Swim, Fly {
    @Override
    public void getName() {
        System.out.println("Дональд");
    }

    @Override
    public void run() {
        System.out.println("Я бегу");
    }

    @Override
    public void swim() {
        System.out.println("Я плаваю");
    }

    @Override
    public void fly() {
        System.out.println("Я летаю");
    }
}
