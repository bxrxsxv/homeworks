package ru.borisov.task7.Animal;

public class Cat extends Animal implements Run, Swim {
    @Override
    public void getName() {
        System.out.println("Матроскин");
    }

    @Override
    public void run() {
        System.out.println("Я бегу");
    }

    @Override
    public void swim() {
        System.out.println("Я плаваю");
    }
}
