package ru.borisov.task7.Animal;

public class App {
    public static void main(String[] args) {
        Cat cat = new Cat();
        Dog dog = new Dog();
        Duck duck = new Duck();

        cat.getName();
        cat.run();
        cat.swim();
        dog.getName();
        dog.run();
        dog.swim();
        duck.getName();
        duck.run();
        duck.swim();
        duck.fly();

    }

}
