package ru.borisov.task7.Person;

public interface Swim {
    void speedUpSwim();
    void slowDownSwim();
}
