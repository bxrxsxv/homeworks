package ru.borisov.task7.Person;

public class App {
    public static void main(String[] args) {
        Person man = new Male();
        Person girl = new Female();

        man.speedUpRun();
        man.slowDownRun();
        man.speedUpSwim();
        man.slowDownSwim();

        girl.speedUpRun();
        girl.slowDownRun();
        girl.speedUpSwim();
        girl.slowDownSwim();
    }
}
