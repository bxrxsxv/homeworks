package ru.borisov.task7.Person;

public interface Run {
    void speedUpRun();
    void slowDownRun();
}
