package ru.borisov.task7.Person;

public class Male extends Person {
    @Override
    public void speedUpRun() {
        System.out.println("Мужчина ускорил бег");
    }

    @Override
    public void slowDownRun() {
        System.out.println("Мужчина замедлил бег");
    }

    @Override
    public void speedUpSwim() {
        System.out.println("Мужчина поплыл быстрее");
    }

    @Override
    public void slowDownSwim() {
        System.out.println("Мужчина поплыл медленнее");
    }
}
