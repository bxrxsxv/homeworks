package ru.borisov.task7.Person;

public class Female extends Person {
    @Override
    public void speedUpRun() {
        System.out.println("Женщина ускорила бег");
    }

    @Override
    public void slowDownRun() {
        System.out.println("Женщина ускорила бег");
    }

    @Override
    public void speedUpSwim() {
        System.out.println("Женщина поплыла быстрее");
    }

    @Override
    public void slowDownSwim() {
        System.out.println("Женщина поплыла медленнее");
    }
}
