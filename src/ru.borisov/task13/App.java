package ru.borisov.task13;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class App {
    private static String s = "Привет, Мир!";
    public static void main(String[] args) throws IOException {
        Path fileOne = Paths.get("C:/Users/Admin/IdeaProjects/Innopolis/src/ru.borisov/task13/fileOne.txt");
        Path fileTwo = Paths.get("C:/Users/Admin/IdeaProjects/Innopolis/src/ru.borisov/task13/fileTwo.txt");

        if (!Files.exists(fileOne)) {
            Files.createFile(fileOne);
            try (OutputStream os = new FileOutputStream(fileOne.toFile())) {
                os.write(s.getBytes("Windows-1251"));
                System.out.println(s);
            }
        }

        try (InputStream is = new FileInputStream(fileOne.toFile());
             OutputStream os = new FileOutputStream(fileTwo.toFile())) {
            byte[] buf = new byte[100];
            for (int i = 0; i < is.available(); i = i + 100) {
                if (is.available() - i >= 100) {
                    is.read(buf, i, 100);
                } else
                    is.read(buf, i, is.available() - i);
                    os.write(new String(buf, "Windows-1251").getBytes("UTF8"));
                    System.out.println(s);
            }
        }
    }
}
