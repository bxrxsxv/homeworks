package ru.borisov.task21;

import java.util.Arrays;

public class Reverse {
    public static void main(String[] args) {
        int[] mass = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        System.out.println(Arrays.toString(mass));

        reverseArray(mass);

        System.out.println(Arrays.toString(mass));
    }

    public static void swap(int[] mass, int ind1, int ind2) {
        int temp=mass[ind1];
        mass[ind1]=mass[ind2];
        mass[ind2]=temp;
    }

    public static int[] reverseArray(int[] mass){
        int size=mass.length-1;

        for(int i=0;i<size;i++){
            swap(mass,i,size--);
        }
        return mass;
    }
}
