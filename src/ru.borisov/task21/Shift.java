package ru.borisov.task21;

import java.util.Arrays;

public class Shift {
    public static void main(String[] args) {
        int[] mass = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        toLeft(mass, 1);
        System.out.println(Arrays.toString(mass));
    }

    public static void toLeft(int[] array, int positions) {
        int size = array.length;

        for (int i = 0; i < positions; i++) {
            int temp = array[0];

            for (int j = 0; j < size-1; j++) {
                array[j] = array[j+1];
            }

            array[size-1] = 0;
        }
    }

}
