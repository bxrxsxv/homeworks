package ru.borisov.task6.dogovorAct;

import lombok.Data;

import java.time.LocalDate;
@Data
public class Converter {
    public static void main(String[] args) {
        Dogovor dr = new Dogovor(1, LocalDate.now(), new String[] {"Вода", "Сок", "Чай"});
        System.out.println(Converter.convert(dr));
    }
    public static Act convert(Dogovor dr) {
        Act act = new Act(dr.getNumber(), LocalDate.now(), dr.getProduct());
        return act;
    }
}
