package ru.borisov.task6.dogovorAct;

import lombok.Data;

import java.time.LocalDate;
@Data
public class Act {
    private int number;
    private LocalDate localDate;
    private String[] product;


    public Act(int number, LocalDate localDate, String[] product) {
        this.number = number;
        this.localDate = localDate;
        this.product = product;
    }
}
