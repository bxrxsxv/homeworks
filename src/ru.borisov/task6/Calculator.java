package ru.borisov.task6;

public class Calculator {
    public static void main(String[] args) {
        System.out.println(Operation.sum(2, 7));
        System.out.println(Operation.sum(7.9, 8.25));
        System.out.println(Operation.substract(7, 2));
        System.out.println(Operation.substract(7.32, 2.76));
        System.out.println(Operation.multiple(7, 2));
        System.out.println(Operation.multiple(7.32, 2.76));
        System.out.println(Operation.division(7, 2));
        System.out.println(Operation.division(7.32, 2.76));
        System.out.println(Operation.percent(7, 2));
        System.out.println(Operation.percent(7.32, 2.76));
    }
}

class Operation {
    static int sum (int x, int y) {
        return x + y;
    }
    static double sum (double x, double y) {
        return x + y;
    }
    static int substract (int x, int y) {
        return x - y;
    }
    static double substract (double x, double y) {
        return x - y;
    }
    static int multiple (int x, int y) {
        return x * y;
    }
    static double multiple (double x, double y) {
        return x * y;
    }
    static int division (int x, int y) {
        return x / y;
    }
    static double division (double x, double y) {
        return x / y;
    }
    static int percent (int x, int y) {
        return x % y;
    }
    static double percent (double x, double y) {
        return x % y;
    }
}


