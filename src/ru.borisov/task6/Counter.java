package ru.borisov.task6;

import java.util.concurrent.Callable;

public class Counter {
    public static void main(String[] args) {
        Car.getCounter();

        Car lada = new Car();
        Car mazda = new Car();
        Car toyota = new Car();
        Car kia = new Car();

        Car.getCounter();
    }
}

class Car {
    private static int counter = 0;

    Car(){
        counter++;
    }
    public static void getCounter(){
        System.out.printf("Counter: %d \n", counter);
    }
}
