package ru.borisov.task4;

import java.time.LocalDate;

public class Visitors extends Person {
    private int id;

    public Visitors(String name, String lastName, LocalDate birtday) {
        super(name, lastName, birtday);
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
