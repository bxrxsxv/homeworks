package ru.borisov.task4;

import lombok.Data;

@Data
public class ReadingRoom {
    private Books[] books = new Books[1000];
    private Visitors[] visitors = new Visitors[10];

}
