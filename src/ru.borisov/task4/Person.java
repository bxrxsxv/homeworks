package ru.borisov.task4;

import lombok.Data;

import java.time.LocalDate;
@Data
public class Person {
    protected String name;
    protected String lastName;
    protected LocalDate birtday;

    public Person(String name, String lastName, LocalDate birtday) {
        this.name = name;
        this.lastName = lastName;
        this.birtday = birtday;
    }

}
