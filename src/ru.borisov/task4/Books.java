package ru.borisov.task4;

import lombok.Data;

@Data
public class Books {
    private String title;
    private String genre;
    private String authors;
    private int year;

    public Books(String title, String genre, String authors, int year) {
        this.title = title;
        this.genre = genre;
        this.authors = authors;
        this.year = year;
    }
}
