package ru.borisov.task4;

import ru.borisov.task4.administration.Accountant;

import java.time.LocalDate;

public class LibraryApp {
    public static void main(String[] args) {
        Visitors ivan = new Visitors("Ivan", "Fadeev",
                LocalDate.of(1999, 01, 12));
        Visitors alexey = new Visitors("Alexey", "Pavlov",
                LocalDate.of(1997, 06, 25));

        ivan.setId(123);
        alexey.setId(765);

        Books wow = new Books(" World of Warcraft Chronicle: Volume 1", " Fantasy",
                " Крис Метцен, Мэтт Бернс, Роберт Брукс",  2018);

        ReadingRoom readingRoom = new ReadingRoom();

        readingRoom.setVisitors(new Visitors[]{ivan});
        readingRoom.setVisitors(new Visitors[]{alexey});
        readingRoom.setBooks(new Books[]{wow});
        System.out.println(alexey);
        System.out.println(ivan);
        System.out.println(wow);

    }
}
