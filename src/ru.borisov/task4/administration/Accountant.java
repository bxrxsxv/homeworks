package ru.borisov.task4.administration;

import lombok.Data;
import ru.borisov.task4.Person;

import java.time.LocalDate;
@Data
public class Accountant extends Person {
    private int salary;

    public Accountant(String name, String lastName, LocalDate birtday, int id) {
        super(name, lastName, birtday);
    }
}
