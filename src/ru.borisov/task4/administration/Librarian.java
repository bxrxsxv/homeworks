package ru.borisov.task4.administration;

import lombok.Data;
import ru.borisov.task4.Person;

import java.time.LocalDate;

@Data
public class Librarian extends Person {
    private int salary;

    public Librarian(String name, String lastName, LocalDate birtday) {
        super(name, lastName, birtday);
    }

    int getSalary() {
        return salary;
    }

    void setSalary(int salary) {
        this.salary = salary;
    }
}
