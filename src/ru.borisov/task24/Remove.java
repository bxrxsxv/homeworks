package ru.borisov.task24;

import java.util.*;

public class Remove {
    Set<String> set = new LinkedHashSet<>();

    public List<String> removeEvenLength(Set<String> set) {
        List<String> list = new ArrayList<>(set);
        for (int i = 0; i < list.size(); i++) {
            String removeSet = list.get(i);
            if (removeSet.length() % 2 == 0) {
                list.remove(i);
                i--;
            }
        }
        return list;
    }
}
