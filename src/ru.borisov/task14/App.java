package ru.borisov.task14;

import java.io.*;
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws FileNotFoundException {

        Scanner inFile = new Scanner(new FileReader(
                "C:\\Users\\Admin\\IdeaProjects\\Innopolis\\src\\ru.borisov\\task14\\products.txt"));
        PrintStream os = new PrintStream(
                "C:\\Users\\Admin\\IdeaProjects\\Innopolis\\src\\ru.borisov\\task14\\out.txt");

        int count = 1;
        float total = 0;

        os.println("Наименование        Цена   Кол-во    Стоимость");
        os.println("===============================================");

        while (inFile.hasNext()) {
            String name = inFile.nextLine();
            boolean isNextFloat = inFile.hasNextFloat();
            float quantity = inFile.nextFloat();
            float price = inFile.nextFloat();
            inFile.nextLine();

            if (isNextFloat) {
                os.printf("%s\t %-10f x %f\t =%f", name, quantity, price, quantity * price);
            } else {
                os.printf("%s\t %-10d x %f\t =%f", name,(int)quantity, price, quantity * price);
            }

            count++;
            total += quantity * price;

        }
        os.println();
        os.println("===============================================");
        os.printf("Итого: = %f", total);
    }
}
