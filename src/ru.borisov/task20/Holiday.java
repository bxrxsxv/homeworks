package ru.borisov.task20;

public class Holiday {
    private String holiday;
    private String date;

    public String getHoliday() {
        return holiday;
    }

    public void setHoliday(String holiday) {
        this.holiday = holiday;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Holiday{" +
                "holiday='" + holiday + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
