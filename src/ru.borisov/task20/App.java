package ru.borisov.task20;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.net.URL;

public class App {
    public static void main(String[] args) {
        try {
            URL url = new URL("https://datazen.katren.ru/calendar/day/");
            try (InputStream is = url.openStream()){

                ObjectMapper objectMapper = new ObjectMapper();
                Holiday value = objectMapper.readValue(url, Holiday.class);
                System.out.println(value);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
