package ru.borisov.task22;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class App {
    public static void main(String[] args) {
        PersonSuperComparator psc = new PersonSuperComparator();
        List<Person> persons = new ArrayList<>();

        Person ivan = new Person(1990, "Ivan");
        Person alex = new Person(1995, "Alex");

        persons.add(ivan);
        persons.add(alex);

        Collections.sort(persons, psc);
        System.out.println(persons);
    }
}
