package ru.borisov.task10;

import org.apache.log4j.Priority;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Scanner;


public class App {
    private static final Logger logger = LoggerFactory.getLogger(App.class);
    private static VendingMachine vendingMachine = new VendingMachine();

    public static void main(String[] args) {
        logger.info("Начало работы программы");
        System.out.println("Выберите напиток из списка: ");
        logger.info("Вывод меню");
        for(String menu : vendingMachine.menu()) {
            System.out.println(menu);
        }

        Scanner scan = new Scanner(System.in);
        logger.info("Вызов метода <printHelp>");
        printHelp();
        while (scan.hasNext()) {
            String command = scan.next();
            switch (command) {
                case "add": {
                    logger.info("Добавление денег");
                    int money = scan.nextInt();
                    processAddMoney(money);
                    break;
                }
                case "get": {
                    logger.info("Выбор напитка");
                    int key = scan.nextInt();
                    processGetDrink(key);
                    break;
                }
                case "end": {
                    logger.info("Завершение программы");
                    processEnd();
                    return;
                }
                default:
                    logger.info("Неверный ввод");
                    System.out.println("Команда не определена");
            }
            scan.nextLine();
        }
    }
    private static void processAddMoney(int money) {
        System.out.println("Текущий баланс: " + vendingMachine.addMoney(money));
    }
    private static void processGetDrink(int number) {
        Drinks drinks = vendingMachine.getDrink(number);
        if(drinks != null) {
            System.out.println("Ммм! " + drinks.getTitle() + "!");
        } else {
            logger.info("Не хватает денег");
            System.out.println("Напиток почему-то не получен...");
        }
    }
    private static void processEnd() {
        System.out.println("Ваша сдача: " + vendingMachine.getChange());
    }

    private static void printHelp() {
        System.out.println("Введите 'add <количество>' для добавления купюр");
        System.out.println("Введите 'get <код напитка>' для получения напитка");
        System.out.println("Введите 'end' для получение сдачи");
    }
}
